require 'rails_helper'

RSpec.describe ProductSerializer, type: :class do
  describe '#to_builder' do
    let(:product) { FactoryBot.create(:product) }
    let(:subject) { ProductSerializer.new(product).to_builder }
    it 'has valid schema' do
      expect(subject.target!).to match_schema(:product)
    end
  end
end