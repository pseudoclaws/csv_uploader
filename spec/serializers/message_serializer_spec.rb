require 'rails_helper'

RSpec.describe MessageSerializer, type: :class do
  describe '#to_builder' do
    let(:message) { 'test message' }
    let(:message_type) { 'success' }
    let(:subject) { MessageSerializer.new(message, message_type).to_builder }
    it 'has valid schema' do
      expect(subject.target!).to match_schema(:message)
    end
  end
end