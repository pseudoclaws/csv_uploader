# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReceiveFile, type: :class do
  let(:subject) { ReceiveFile.call(params) }
  let(:params) do
    {
      file: file
    }
  end

  context 'with valid params' do
    context 'suppliers' do
      let(:file) do
        Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'suppliers.csv'), 'text/x-csv', false)
      end
      it 'saves file successfully' do
        expect(subject.success?).to eq true
      end

      it 'creates new attachment' do
        expect { subject }.to change { Attachment.count }.by(1)
      end
    end

    context 'products' do
      let(:file) do
        Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'suppliers.csv'), 'text/x-csv', false)
      end

      it 'saves file successfully' do
        expect(subject.success?).to eq true
      end

      it 'creates new attachment' do
        expect { subject }.to change { Attachment.count }.by(1)
      end
    end
  end

  context 'with invalid params' do
    context 'with invalid file name' do
      let(:file) do
        Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'unknown_content.csv'), 'text/x-csv', false)
      end

      it "doesn't save file" do
        expect(subject.failure?).to eq true
      end

      it "doesn't create new attachment" do
        expect { subject }.not_to change { Attachment.count }
      end
    end

    context 'without attachment' do
      let(:file) do
        nil
      end

      it "doesn't save file" do
        expect(subject.failure?).to eq true
      end

      it "doesn't create new attachment" do
        expect { subject }.not_to change { Attachment.count }
      end
    end
  end
end