require 'rails_helper'

RSpec.describe ParseFile::SupplierRowParser, type: :class do
  let(:params) { [100, FFaker::Product.product_name] }

  let(:subject) { described_class.new(params) }
  it { expect(subject.parse[:code]).to eq params[0] }
  it { expect(subject.parse[:name]).to eq params[1] }
end