require 'rails_helper'

RSpec.describe ParseFile::RowParserBase, type: :class do
  let(:row) { FactoryBot.create(:supplier) }
  let(:subject) { described_class.new(row) }
  it { expect(subject.row).to eq row }
  it { expect { subject.parse }.to raise_error NotImplementedError }
end