require 'rails_helper'

RSpec.describe ParseFile::ProductForm, type: :class do
  let!(:subject) { described_class.new(Product.new) }
  context 'with valid params' do
    let(:params) do
      {
        sku: '1978',
        supplier_code: '2437',
        name: 'Onesync GPS Power Kit',
        additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
        price: '282.97'
      }
    end

    it { expect(subject.validate(params)).to eq true }
    it do
      FactoryBot.create(:supplier, code: '2437', name: 'Kovacek-Dietrich')
      expect { subject.validate(params) && subject.save }.to change { Product.count }.by(1)
    end
  end

  context 'with invalid params' do
    context 'with invalid sku' do
      context 'with blank sku' do
        let(:params) do
          {
            supplier_code: '2437',
            name: 'Onesync GPS Power Kit',
            additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
            price: '282.97'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly "Sku can't be blank"
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
      end
    end
    context 'with invalid supplier_code' do
      context 'with symbolic supplier_code' do
        let(:params) do
          {
            sku: '1978',
            supplier_code: 'supplier',
            name: 'Onesync GPS Power Kit',
            additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
            price: '282.97'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly 'Supplier code is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
      end

      context 'with blank supplier_code' do
        let(:params) do
          {
            sku: '1978',
            name: 'Onesync GPS Power Kit',
            additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
            price: '282.97'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly "Supplier code can't be blank", 'Supplier code is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
      end
    end
    context 'with invalid price' do
      context 'with symbolic price' do
        let(:params) do
          {
            sku: '1978',
            supplier_code: '2437',
            name: 'Onesync GPS Power Kit',
            additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
            price: 'price'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly 'Price is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
      end

      context 'with blank price' do
        let(:params) do
          {
            sku: '1978',
            supplier_code: '2437',
            name: 'Onesync GPS Power Kit',
            additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil]
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly "Price can't be blank", 'Price is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
      end
    end

    context 'with invalid name' do
      let(:params) do
        {
          sku: '1978',
          supplier_code: '2437',
          additional_info: ['GFM', '407', '1-976-976-6558 x867', '607074511915', nil],
          price: '282.97'
        }
      end
      it { expect(subject.validate(params)).to eq false }
      it do
        subject.validate(params)
        expect(subject.errors.full_messages).to contain_exactly "Name can't be blank"
      end
      it { expect { subject.validate(params) && subject.save }.not_to change { Product.count } }
    end
  end
end