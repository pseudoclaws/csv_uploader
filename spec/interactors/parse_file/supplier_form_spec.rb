require 'rails_helper'

RSpec.describe ParseFile::SupplierForm, type: :class do
  let!(:subject) { described_class.new(Supplier.new) }
  context 'with valid params' do
    let(:params) do
      {
        code: '0975',
        name: 'Sporer Inc'
      }
    end

    it { expect(subject.validate(params)).to eq true }
    it { expect { subject.validate(params) && subject.save }.to change { Supplier.count }.by(1) }
  end

  context 'with invalid params' do
    context 'with invalid code' do
      context 'with symbolic code' do
        let(:params) do
          {
            code: 'code',
            name: 'Sporer Inc'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly 'Code is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Supplier.count } }
      end

      context 'with blank code' do
        let(:params) do
          {
            name: 'Sporer Inc'
          }
        end

        it { expect(subject.validate(params)).to eq false }
        it do
          subject.validate(params)
          expect(subject.errors.full_messages).to contain_exactly "Code can't be blank", 'Code is not a number'
        end
        it { expect { subject.validate(params) && subject.save }.not_to change { Supplier.count } }
      end
    end

    context 'with invalid name' do
      let(:params) do
        {
          code: '0975'
        }
      end
      it { expect(subject.validate(params)).to eq false }
      it do
        subject.validate(params)
        expect(subject.errors.full_messages).to contain_exactly "Name can't be blank"
      end
      it { expect { subject.validate(params) && subject.save }.not_to change { Supplier.count } }
    end
  end
end