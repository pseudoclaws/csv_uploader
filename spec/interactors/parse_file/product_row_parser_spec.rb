require 'rails_helper'

RSpec.describe ParseFile::ProductRowParser, type: :class do
  let(:params) { ['1978','2437','Onesync GPS Power Kit','GFM','407','1-976-976-6558 x867','607074511915',nil,'282.97'] }

  let(:subject) { described_class.new(params) }
  it { expect(subject.parse[:sku]).to eq params[0] }
  it { expect(subject.parse[:supplier_code]).to eq params[1] }
  it { expect(subject.parse[:name]).to eq params[2] }
  it { expect(subject.parse[:additional_info]).to eq params[3..7] }
  it { expect(subject.parse[:price]).to eq params[8] }
end