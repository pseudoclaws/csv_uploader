# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ParseFile, type: :class do
  let(:subject) { ParseFile.call(params) }
  let(:params) do
    {
      attachment: attachment
    }
  end

  context 'with valid params' do
    context 'suppliers' do
      let(:attachment) do
        FactoryBot.create(:suppliers_attachment)
      end

      it 'parses file successfully' do
        expect(subject.success?).to eq true
      end

      it 'creates new suppliers' do
        expect { subject }.to change { Supplier.count }
      end
    end

    context 'products' do
      before do
        ParseFile.call(attachment: FactoryBot.create(:suppliers_attachment))
      end

      let(:attachment) do
        FactoryBot.create(:products_attachment)
      end

      it 'parses file successfully' do
        expect(subject.success?).to eq true
      end

      it 'creates new products' do
        expect { subject }.to change { Product.count }
      end
    end
  end

  context 'with invalid params' do
    context 'with invalid file name' do
      let(:attachment) { FactoryBot.create(:unknown_attachment) }
      it 'has failure status' do
        expect(subject.failure?).to eq true
      end
      it 'return errors' do
        expect(subject.errors).not_to be_empty
      end
      it 'returns error message' do
        expect(subject.errors.full_messages.first).to eq 'Unknown file type'
      end
      it "doesn't create new suppliers" do
        expect { subject }.not_to change { Supplier.count }
      end
      it "doesn't create new products" do
        expect { subject }.not_to change { Product.count }
      end
    end

    context 'without attachment' do
      let(:attachment) { nil }
      it 'has failure status' do
        expect(subject.failure?).to eq true
      end
      it 'return errors' do
        expect(subject.errors).not_to be_empty
      end
      it 'returns error message' do
        expect(subject.errors.full_messages.first).to eq "Attachment can't be blank"
      end
      it "doesn't create new suppliers" do
        expect { subject }.not_to change { Supplier.count }
      end
      it "doesn't create new products" do
        expect { subject }.not_to change { Product.count }
      end
    end
  end
end