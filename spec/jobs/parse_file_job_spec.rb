require 'rails_helper'

RSpec.describe ParseFileJob do
  before do
    ActiveJob::Base.queue_adapter.enqueued_jobs = []
  end

  let(:attachment) { FactoryBot.create(:suppliers_attachment) }

  it 'matches with enqueued job' do
    ParseFileJob.perform_later(attachment)
    expect(ParseFileJob).to have_been_enqueued
  end

  it 'enqueues UINotificationJob on success' do
    ParseFileJob.perform_now(attachment)
    expect(UINotificationJob).to have_been_enqueued.with(message: 'File parsing complete!', message_type: 'success')
  end

  it 'enqueues UINotificationJob on failure' do
    ParseFileJob.perform_now(nil)
    expect(UINotificationJob).to have_been_enqueued.with(message: "Attachment can't be blank")
  end
end