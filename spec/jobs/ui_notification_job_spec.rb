require 'rails_helper'

RSpec.describe UINotificationJob do
  before do
    ActiveJob::Base.queue_adapter.enqueued_jobs = []
  end

  it 'matches with enqueued job' do
    UINotificationJob.perform_later(message: 'test message')
    expect(UINotificationJob).to have_been_enqueued
  end
end