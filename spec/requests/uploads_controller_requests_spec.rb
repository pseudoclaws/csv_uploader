require 'rails_helper'

RSpec.describe 'Requests to UploadsController', type: :request do
  describe 'GET /' do
    let(:subject) { get '/' }
    it 'responses with ok status' do
      subject
      expect(response).to be_ok
    end
  end

  describe 'POST /uploads' do
    let(:subject) { post '/uploads', params: params, headers: { 'Accept': '*/*' } }
    context 'with valid parameters' do
      let(:params) do
        {
          file: Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'suppliers.csv'), 'text/x-csv', false)
        }
      end

      it 'responses with created status' do
        subject
        expect(response).to have_http_status(:created)
      end

      it 'creates new attachment' do
        expect { subject }.to change { Attachment.count }.by(1)
      end
    end

    context 'with invalid parameters' do
      shared_context 'attachment creation failure' do
        it 'responses with unprocessable_entity status' do
          subject
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it "doesn't create new attachment" do
          expect { subject }.not_to change { Attachment.count }
        end
      end

      context 'with invalid file name' do
        let(:params) do
          {
            file: Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'unknown_content.csv'), 'text/x-csv', false)
          }
        end
        it_behaves_like 'attachment creation failure'
      end

      context 'without file' do
        let(:params) do
          {}
        end
        it_behaves_like 'attachment creation failure'
      end
    end
  end
end