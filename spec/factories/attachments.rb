# frozen_string_literal: true

FactoryBot.define do
  factory :products_attachment, class: Attachment do
    file File.open(Rails.root.join('spec', 'fixtures', 'files', 'sku.csv'))
  end

  factory :suppliers_attachment, class: Attachment do
    file File.open(Rails.root.join('spec', 'fixtures', 'files', 'suppliers.csv'))
  end

  factory :unknown_attachment, class: Attachment do
    file File.open(Rails.root.join('spec', 'fixtures', 'files', 'unknown_content.csv'))
  end
end
