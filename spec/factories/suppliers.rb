# frozen_string_literal: true

FactoryBot.define do
  factory :supplier do
    code { 1234 }
    name FFaker::Company.name
  end
end
