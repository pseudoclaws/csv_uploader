# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    sku 'T0123'
    supplier
    name FFaker::Product.product_name
    price 200.0
    additional_info []
  end
end
