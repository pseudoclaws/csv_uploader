# frozen_string_literal: true

class XssFilter
  def self.call(fragment, _options = {})
    return fragment unless fragment.is_a?(String)
    ActionController::Base.helpers.strip_tags(fragment)
  end
end
