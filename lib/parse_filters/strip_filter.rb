# frozen_string_literal: true

class StripFilter
  def self.call(fragment, _options = {})
    fragment.is_a?(String) ? fragment.strip.presence : fragment
  end
end
