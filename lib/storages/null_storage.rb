# frozen_string_literal: true

module Storages
  # NullStorage provider for CarrierWave for use in tests.  Doesn't actually
  # upload or store files but allows test to pass as if files were stored and
  # the use of fixtures.
  class NullStorage
    attr_reader :uploader

    def initialize(uploader)
      @uploader = uploader
    end

    def identifier
      uploader.filename
    end

    def store!(_file)
      true
    end

    def retrieve!(_identifier)
      file = Rails.root.join('spec', 'fixtures', 'files', 'suppliers.csv')
      tmp = Rails.root.join('tmp', 'suppliers.csv')
      FileUtils.cp(file, tmp)
      CarrierWave::SanitizedFile.new(tmp)
    end
  end
end
