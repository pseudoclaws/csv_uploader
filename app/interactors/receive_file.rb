# frozen_string_literal: true

class ReceiveFile < InteractorsBase
  include Interactor

  def call
    context.fail!(errors: form.errors) unless form.validate(context.to_h)
    context.fail!(errors: form.errors) unless form.save
    context.form = form
    ParseFileJob.perform_later(context.form.model)
    UINotificationJob.perform_later(message: I18n.t('notifications.file_uploaded'), message_type: 'success')
  end

  private

  def form
    @form ||= Form.new(Attachment.new)
  end
end