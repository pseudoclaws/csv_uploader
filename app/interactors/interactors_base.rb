# frozen_string_literal: true

class InteractorsBase
  include Interactor

  SUPPLIERS_FILE_NAME = 'suppliers.csv'
  PRODUCTS_FILE_NAME = 'sku.csv'

  VALID_NAMES = [SUPPLIERS_FILE_NAME, PRODUCTS_FILE_NAME].freeze
end