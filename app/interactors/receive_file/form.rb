# frozen_string_literal: true

class ReceiveFile
  class Form < ::Reform::Form
    model Attachment
    property :file

    validates :file, presence: true
    validate :name_is_valid?, if: -> { file.present? }

    private

    def name_is_valid?
      return if InteractorsBase::VALID_NAMES.include?(file.original_filename)
      errors.add(:base, I18n.t('activerecord.errors.messages.unknown_file_type'))
    end
  end
end