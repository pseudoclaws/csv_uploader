# frozen_string_literal: true

class ParseFile
  class ProductRowParser < RowParserBase
    def parse
      {
        sku: row[0],
        supplier_code: row[1],
        name: row[2],
        additional_info: row[3..7],
        price: row[8]
      }
    end
  end
end