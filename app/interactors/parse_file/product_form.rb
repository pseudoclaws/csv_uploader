# frozen_string_literal: true

class ParseFile
  class ProductForm < Reform::Form
    model Product
    properties :sku, :supplier_code, :name, :price, :additional_info, parse_filter: [StripFilter, XssFilter]
    validates :sku, :supplier_code, :name, :price, presence: true
    validates :price, numericality: true
    validates :supplier_code, numericality: { only_integer: true }
  end
end