# frozen_string_literal: true

class ParseFile
  class SupplierForm < Reform::Form
    model Supplier
    properties :code, :name, parse_filter: [StripFilter, XssFilter]
    validates :code, :name, presence: true
    validates :code, numericality: { only_integer: true }
  end
end