# frozen_string_literal: true

class ParseFile
  class RowParserBase
    attr_reader :row

    def initialize(row)
      @row = row
    end

    def parse
      raise NotImplementedError, 'this method should be implemented in descendants'
    end
  end
end