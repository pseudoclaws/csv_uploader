# frozen_string_literal: true

class ParseFile
  class SupplierRowParser < RowParserBase
    def parse
      {
        code: row[0],
        name: row[1]
      }
    end
  end
end