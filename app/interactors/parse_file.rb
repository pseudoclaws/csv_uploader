# frozen_string_literal: true

require 'csv'

class ParseFile < InteractorsBase
  include Interactor

  class ParsingError < RuntimeError; end

  attr_reader :output

  def call
    validate_attachment
    validate_file_name
    parse
    save
  end

  private

  def errors
    @errors ||= ActiveModel::Errors.new(self)
  end

  def file_name
    @file_name ||= File.basename(context.attachment.file.path)
  end

  def set_form
    case file_name
    when SUPPLIERS_FILE_NAME
      ParseFile::SupplierForm.new(Supplier.new)
    when PRODUCTS_FILE_NAME
      ParseFile::ProductForm.new(Product.new)
    end
  end

  def conflict_target
    @conflict_target ||= case file_name
                         when SUPPLIERS_FILE_NAME
                           %i[code]
                         when PRODUCTS_FILE_NAME
                           %i[sku supplier_code]
                         end
  end

  def columns
    @columns ||= case file_name
                 when SUPPLIERS_FILE_NAME
                   %i[name]
                 when PRODUCTS_FILE_NAME
                   %i[name price]
                 end
  end

  def parse
    prev_thousand = 0
    @output = CSV.parse(File.open(context.attachment.file.path), col_sep: '¦').each_with_index.inject([]) do |output, (row, i)|
      form = set_form
      form.validate(parse_row(row)) && form.save { |form_hash| output << form_hash }
      thousand = i / 1000
      if thousand > prev_thousand
        prev_thousand = thousand
        UINotificationJob.perform_later(message: "Parsed #{i} records", message_type: 'success')
      end
      output
    end
  end

  def save
    set_form.model.class.import(output, on_duplicate_key_update: {conflict_target: conflict_target, columns: columns}) unless output.empty?
  end

  def parse_row(row)
    case file_name
    when SUPPLIERS_FILE_NAME
      ParseFile::SupplierRowParser.new(row)
    when PRODUCTS_FILE_NAME
      ParseFile::ProductRowParser.new(row)
    end.parse
  end

  def validate_file_name
    return if %w[suppliers.csv sku.csv].include? file_name
    errors.add(:base, I18n.t('activerecord.errors.messages.unknown_file_type'))
    context.fail!(errors: errors)
  end

  def validate_attachment
    if context.attachment.blank?
      errors.add(:base, I18n.t('activerecord.errors.messages.blank_attachment'))
      context.fail!(errors: errors)
      return
    end
    return if File.exist? context.attachment.file.path
    errors.add(:base, I18n.t('activerecord.errors.messages.blank_attachment_file'))
    context.fail!(errors: errors)
  end
end