# frozen_string_literal: true

class MessageSerializer
  attr_reader :message, :message_type

  def initialize(message, message_type)
    @message = message
    @message_type = message_type
  end

  def to_builder
    Jbuilder.new do |json|
      json.message do
        json.message message
        json.type message_type
      end
    end
  end
end