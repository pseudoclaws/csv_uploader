class ProductsListSerializer
  attr_reader :products

  def initialize(products)
    @products = products
  end

  def call
    products.map do |product|
      ProductSerializer.new(product).to_builder.attributes!
    end
  end
end