# frozen_string_literal: true

class ProductSerializer
  attr_reader :product

  def initialize(product)
    @product = product
  end

  def to_builder
    Jbuilder.new do |json|
      json.call(product, :id, :sku, :name, :price, :additional_info)
      json.supplier_name product.try(:supplier_name) || product.supplier.name
    end
  end
end