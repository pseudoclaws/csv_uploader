# frozen_string_literal: true

class RootSerializer
  attr_reader :root

  def initialize(root)
    @root = root
  end

  def to_builder
    Jbuilder.new do |json|
      root.each do |key, builder|
        json.set! key, builder
      end
    end
  end
end