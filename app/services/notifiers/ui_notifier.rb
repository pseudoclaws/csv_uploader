# frozen_string_literal: true

module Notifiers
  class UINotifier
    def self.call(data)
      ActionCable.server.broadcast('ui_notifications', data, coder: JbuilderEncoder)
    end
  end
end