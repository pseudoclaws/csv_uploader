# frozen_string_literal: true

class JbuilderEncoder
  def self.encode(value, _options = {})
    value
  end

  def self.decode(value, _options = {})
    MultiJson.load(value)
  end
end
