class Attachment < ApplicationRecord
  mount_uploader :file, DataFileUploader
end
