class Supplier < ApplicationRecord
  has_many :products, inverse_of: :supplier, primary_key: :code, foreign_key: :supplier_code
end
