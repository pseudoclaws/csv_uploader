class Product < ApplicationRecord
  belongs_to :supplier, foreign_key: :supplier_code, primary_key: :code, inverse_of: :products
end
