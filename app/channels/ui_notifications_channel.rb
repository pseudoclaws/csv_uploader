# frozen_string_literal: true

class UINotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'ui_notifications'
  end
end
