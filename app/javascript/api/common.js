'use strict';

import fetch from 'isomorphic-fetch'

export function myFetch(url, method = 'GET', body = null) {
  const headers = {
    'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'X-Requested-With': 'XMLHttpRequest'
  };
  return fetch(url, {
    method,
    body,
    headers,
    credentials: 'same-origin'
  })
}

export function myJsonFetch(url, method = 'GET', body = null) {
  const headers = {
    'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'X-Requested-With': 'XMLHttpRequest',
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
  const options = {
    method,
    headers,
    credentials: 'same-origin'
  };
  if(body) options.body = JSON.stringify(body);
  return fetch(url, options)
}

export function guardedFetch(url, method = 'GET', body = null) {
  return myJsonFetch(url, method, body).then(
    response => {
      return response.json().then(json => ({
        status: response.status,
        json
      }));
    }
  ).then(({status, json}) => {
    if (status >= 400)
    {
      throw new FetchError(json.errors[0])
    }
    else
    {
      return Promise.resolve(json)
    }
  }).catch(error => {
    throw new FetchError(error.message === 'Failed to fetch' ? 'Server unavailable' : error.message)
  })
}

export function guardedFDFetch(url, method = 'GET', formData) {
  return myFetch(url, method, formData).then(response => {
      return response.json().then(json => ({
        status: response.status,
        json
      }));
    }
  ).then(({status, json}) => {
    if (status >= 400)
    {
      throw new FetchError(json.errors[0])
    }
    else
    {
      return Promise.resolve(json)
    }
  }).catch(error => {
    throw new FetchError(error.message === 'Failed to fetch' ? 'Server unavailable' : error.message)
  })
}

function FetchError(message) {
  this.message = message;
}