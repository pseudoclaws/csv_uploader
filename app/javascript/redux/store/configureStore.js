import { applyMiddleware, compose, createStore } from 'redux';
import reducer from '../reducers';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk'

const loggerMiddleware = createLogger();

let finalCreateStore = compose(
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
)(createStore);

export default function configureStore(initialState = {}) {
  return finalCreateStore(reducer, initialState);
}