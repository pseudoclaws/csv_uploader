'use strict';

import {guardedFDFetch} from "../../api/common";
import {addAlert} from "./alerts/alerts";

export function uploadFile(file) {
  return function (dispatch) {
    const formData = new FormData;
    formData.append('file', file);
    guardedFDFetch('/uploads', 'post', formData).then(
      dispatch(addAlert({type: 'success', message: 'File was uploaded successfully'}))
    )
  }
}