'use strict';

import {ADD_PRODUCT, ADD_PRODUCTS} from "./productTypes";
import {guardedFetch} from "../../../api/common";

export function addProduct(product) {
  return {
    type: ADD_PRODUCT,
    product
  }
}

export function addProducts(products) {
  return {
    type: ADD_PRODUCTS,
    products
  }
}

export function downloadProductsPage(page) {
  return function (dispatch) {
    guardedFetch(`/?page=${page}`, 'get').then(
      data => dispatch(addProducts(data.products))
    )
  }
}