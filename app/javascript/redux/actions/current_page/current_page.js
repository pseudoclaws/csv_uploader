'use strict';

import { SET_CURRENT_PAGE } from "./currentPageTypes"

export function setCurrentPage(current_page) {
  return {
    type: SET_CURRENT_PAGE,
    current_page
  }
}