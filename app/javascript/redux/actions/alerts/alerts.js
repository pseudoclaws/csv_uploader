'use strict';

import {SET_ALERTS, REMOVE_ALERT, ADD_ALERT} from "./alertTypes";

export function setAlerts(alerts) {
  return {
    type: SET_ALERTS,
    alerts
  }
}

export function addAlert(alert) {
  return {
    type: ADD_ALERT,
    alert
  }
}

export function removeAlert(alert)
{
  return {
    type: REMOVE_ALERT,
    alert
  }
}