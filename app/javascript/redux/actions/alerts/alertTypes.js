'use strict';

export const SET_ALERTS = 'SET_ALERTS';
export const ADD_ALERT = 'ADD_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';