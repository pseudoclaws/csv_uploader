'use strict';

import current_page from './current_page'
import products from './products'
import alerts from './alerts'

import { combineReducers } from 'redux'

export default combineReducers(
  {
    current_page,
    products,
    alerts
  }
);