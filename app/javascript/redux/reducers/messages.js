'use strict';

import {ADD_NOTIFICATION, REMOVE_NOTIFICATION} from "../actions/messages/messageTypes";

export default function (state = [], action) {
  switch (action.type) {
    case ADD_NOTIFICATION:
      return [...state, action.notification];
    case REMOVE_NOTIFICATION:
      return [...state.slice(0, state.length - 1)];
    default: return true;
  }
}