'use strict';

import {ADD_PRODUCT, ADD_PRODUCTS} from "../actions/products/productTypes";

export default function (state = [], action) {
  switch (action.type) {
    case ADD_PRODUCTS:
      return [...state, ...action.products];
    case ADD_PRODUCT:
      const idx = state.findIndex(p => p.id === action.product.id);
      if (idx >= 0) return state;
      return [...state, action.product];
    default: return state;
  }
}