'use strict';

import {REMOVE_ALERT, ADD_ALERT} from "../actions/alerts/alertTypes";

export default function (state = [], action) {
  switch (action.type)
  {
    case ADD_ALERT:
      return [...state, Object.assign({id: new Date().getTime(), type: 'danger'}, action.alert)];
    case REMOVE_ALERT:
      const idx = state.findIndex(a => a.id === action.alert.id);
      if (idx < 0) return state;
      return [...state.slice(0, idx), ...state.slice(idx + 1)];
    default:
      return state;
  }
}