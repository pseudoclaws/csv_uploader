'use strict';

import { SET_CURRENT_PAGE } from "../actions/current_page/currentPageTypes"

export default function current_page(state = '', action) {
  switch (action.type)
  {
    case SET_CURRENT_PAGE:
      return action.current_page;
    default:
      return state;
  }
}