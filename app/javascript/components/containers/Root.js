'use strict';

import React from 'react'
import { Provider } from 'react-redux'

import ActionCable from 'actioncable'

import { setCurrentPage } from '../../redux/actions/current_page/current_page'

import store from '../../redux/store'

import ProductTableApp from './UploadsPageApp'
import AlertsApp from './AlertsApp'

import {addProduct, addProducts} from "../../redux/actions/products/products";
import {addAlert} from "../../redux/actions/alerts/alerts";

const cable = ActionCable.createConsumer();

cable.subscriptions.create("UINotificationsChannel", {
  received: (data) => {
    if (data.hasOwnProperty('product'))
    {
      store.dispatch(addProduct(data.product))
    }
    if (data.hasOwnProperty('message'))
    {
      store.dispatch(addAlert(data.message))
    }
  }
});

export default class Root extends React.Component
{
  render()
  {
    let PageContainer = null;
    switch (this.props.current_page)
    {
      case 'uploads#index':
        PageContainer = ProductTableApp;
        break;
    }
    return (
      <Provider store={store}>
        <React.Fragment>
          <PageContainer cable={cable}/>
          <AlertsApp/>
        </React.Fragment>
      </Provider>
    )
  }

  componentWillMount()
  {
    store.dispatch(setCurrentPage(this.props.current_page));
    switch (this.props.current_page)
    {
      case 'uploads#index':
        store.dispatch(addProducts(this.props.products));
        break;
    }
  }
}