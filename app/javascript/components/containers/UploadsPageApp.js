'use strict';

import { connect } from 'react-redux'

import UploadsPage from "../uploads/UploadsPage";

const mapStateToProps = state => {
  return {
    products: state.products
  }
};

export default connect(mapStateToProps)(UploadsPage);