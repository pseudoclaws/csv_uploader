'use strict';

import { connect } from 'react-redux';
import AlertList from '../alerts/AlertListContainer'

function mapStateToProps(state) {
  return {
    alerts: state.alerts
  }
}

export default connect(mapStateToProps)(AlertList);