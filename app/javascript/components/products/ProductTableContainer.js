'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import ProductTableRow from "./ProductTableRow";
import Waypoint from 'react-waypoint'

import store from '../../redux/store'
import {downloadProductsPage} from "../../redux/actions/products/products";
import ProductTable from "./ProductTable";

export default class ProductTableContainer extends React.Component{
  constructor(props)
  {
    super(props);
    this.state = {
      page: 1
    };
    this.downloadNextPage = this.downloadNextPage.bind(this);
  }

  render()
  {
    return <ProductTable products={this.props.products} downloadNextPage={this.downloadNextPage}/>
  }

  downloadNextPage()
  {
    this.setState({page: this.state.page + 1});
    store.dispatch(downloadProductsPage(this.state.page));
  }
}