'use strict';

import React from 'react'
import PropTypes from 'prop-types'

export default class ProductTableRow extends React.PureComponent{
  render()
  {
    return (
      <tr>
        <td>{this.props.row.sku}</td>
        <td>{this.props.row.name}</td>
        <td>{this.props.row.supplier_name}</td>
        {
          this.props.row.additional_info.map((info, i) => {
            return <td key={i}>{info}</td>
          })
        }
        <td>{this.props.row.price}</td>
      </tr>
    )
  }
}

ProductTableRow.propTypes = {
  row: PropTypes.shape(
    {
      id: PropTypes.number.isRequired,
      sku: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      supplier_name: PropTypes.string.isRequired,
      additional_info: PropTypes.array.isRequired,
      price: PropTypes.string.isRequired,
    }
  ).isRequired
};