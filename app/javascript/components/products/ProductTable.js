'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import ProductTableRow from "./ProductTableRow";
import Waypoint from 'react-waypoint'

export default class ProductTable extends React.PureComponent{
  render()
  {
    return (
      <React.Fragment>
        <table className="table">
          <thead>
          <tr>
            <th>SKU</th>
            <th>Name</th>
            <th>Supplier</th>
            {
              ProductTable.additionalFieldsHeader()
            }
            <th>Price</th>
          </tr>
          </thead>
          <tbody>
          {
            this.props.products.map(product => {
              return <ProductTableRow key={product.id} row={product}/>
            })
          }
          </tbody>
        </table>
        <Waypoint onEnter={this.props.downloadNextPage}/>
      </React.Fragment>
    )
  }

  static additionalFieldsHeader()
  {
    const output = [];
    for (let i = 0; i < 5; i++) {
      output.push(
        <th key={i}/>
      )
    }
    return output;
  }
}

ProductTable.propTypes = {
  products: PropTypes.array.isRequired,
  downloadNextPage: PropTypes.func.isRequired
};