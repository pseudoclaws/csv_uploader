'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import ProductTableContainer from "../products/ProductTableContainer";

import store from '../../redux/store'
import {uploadFile} from "../../redux/actions/file";

export default class UploadsPage extends React.PureComponent{
  constructor(props)
  {
    super(props);
    this.uploadFile = this.uploadFile.bind(this);
  }

  render()
  {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-12">
            <input ref="file_input" type="file" onChange={this.uploadFile}/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <ProductTableContainer products={this.props.products}/>
          </div>
        </div>
      </React.Fragment>
    )
  }

  uploadFile()
  {
    store.dispatch(uploadFile(this.refs.file_input.files[0]))
  }
}

UploadsPage.propTypes = {
  products: PropTypes.array.isRequired
};