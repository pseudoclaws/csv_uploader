'use strict';

import React from 'react'
import { AlertList } from 'react-bs-notifier'

import store from '../../redux/store'
import {removeAlert} from '../../redux/actions/alerts/alerts'

export default function (props) {
  return (
    <AlertList {...props} timeout={1000} dismissTitle="Close" onDismiss={alert => store.dispatch(removeAlert(alert))}/>
  )
}