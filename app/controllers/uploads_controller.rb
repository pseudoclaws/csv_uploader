# frozen_string_literal: true

class UploadsController < ApplicationController
  include JsonSerialization

  def index
    @data = serialize(
      products: ProductsListSerializer.new(
        Product.select('products.*, suppliers.name as supplier_name').joins(:supplier).page(params[:page])
      ).call
    )
    respond_to do |format|
      format.html
      format.json { render json: @data }
    end
  end

  def create
    result = ReceiveFile.call(file: params[:file])
    if result.success?
      render json: serialize(attachment: result.form.model), status: :created
    else
      pp result.errors.full_messages
      render json: { errors: result.errors.full_messages }, status: :unprocessable_entity
    end
  end
end
