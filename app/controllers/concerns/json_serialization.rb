# frozen_string_literal: true

module JsonSerialization
  extend ActiveSupport::Concern

  def serialize(root)
    builder = RootSerializer.new(root).to_builder
    builder.merge!(
      current_page: "#{controller_path}##{action_name}"
    )
    builder.target!
  end
end