# frozen_string_literal: true

class UINotificationJob < ApplicationJob
  queue_as :ui

  def perform(model: nil, message: nil, message_type: :danger)
    send_model(model)
    send_message(message, message_type)
  end

  private

  def send_model(model)
    data = model_data(model)
    Notifiers::UINotifier.call(data) if data.present?
  end

  def send_message(message, message_type)
    data = message_data(message, message_type)
    Notifiers::UINotifier.call(data) if data.present?
  end

  def model_data(model)
    return if model.blank?
    MultiJson.dump(
      model.class.name.downcase => "#{model.class.name}Serializer".constantize&.new(model)&.to_builder&.attributes!
    )
  end

  def message_data(message, message_type)
    return if message.blank?
    MessageSerializer.new(message, message_type).to_builder.target!
  end
end