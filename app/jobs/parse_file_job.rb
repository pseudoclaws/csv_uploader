# frozen_string_literal: true

class ParseFileJob < ApplicationJob
  discard_on(ParseFile::ParsingError) do |_, error|
    UINotificationJob.perform_later(message: error.message)
  end

  def perform(attachment)
    result = ParseFile.call(attachment: attachment)
    UINotificationJob.perform_later(message: 'File parsing complete!', message_type: 'success') && return if result.success?
    raise ParseFile::ParsingError, result.errors.full_messages.first
  end
end