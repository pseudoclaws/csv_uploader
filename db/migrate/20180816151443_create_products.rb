class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :sku
      t.string :supplier_code, index: true
      t.string :name
      t.decimal :price, precision: 8, scale: 2
      t.string :additional_info, array: true

      t.timestamps
    end
    add_index :products, %i[sku supplier_code], unique: true
    add_foreign_key :products, :suppliers, column: :supplier_code, primary_key: :code
  end
end
